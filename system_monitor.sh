#!/bin/sh

# from http://ideatrash.net/2011/02/making-geektool-little-easier-on-system.html
# with modifications

#system statistics

# battery

battery=`ioreg -l | grep -i capacity | tr '\n' ' | ' | awk '{printf("%.2f%%", $10/$5 * 100)}'`

echo "battery\t\t$battery"

#we run it once here to get physical memory free
top -n1 -l 1| awk '/PhysMem/ {print "ram free\t" $6 " "}' # modified

#get data from top - we just want header info
#run it twice so we get real CPU percentages
topdata=`top -n1 -l 2`; 
echo "$topdata"|awk '/CPU usage/ && NR > 5 {print "cpu user\t" $3}'
echo "$topdata"|awk '/CPU usage/ && NR > 5 {print "cpu sys\t\t" $5}'
echo "$topdata"|awk '/CPU usage/ && NR > 5 {print "cpu idle\t" $7}'
#end


#wifi
myen0=`ifconfig en0 | grep "inet " | grep -v 127.0.0.1 | awk '{print $2}'`

if [ "$myen0" != "" ]
then
echo "airport\t\t$myen0"
else
echo "airport\t\tINACTIVE"
fi

myssid=`/System/Library/PrivateFrameworks/Apple80211.framework/Versions/Current/Resources/airport -I | awk '/ SSID/ {print substr($0, index($0, $2))}'`

if [ "$myssid" != "" ]
then
echo "network\t\t$myssid"
else
echo "no network"
fi

inetip=$(curl -s http://checkip.dyndns.org/ | sed 's/[a-zA-Z<>/ :]//g')
if [ "$inetip" = "" ]
then 
inetip="offline"
fi
echo "ext ip\t\t$inetip"

#end

#uptime info
updata=`uptime`
echo "$updata" | awk '{print "uptime\t\t" $3 " " $4 " "$5" " }'
echo "$updata" | awk '{print "load\t\t" $8 " "$9" "$10" "$11" "}'
#end
