#!/bin/sh

# world clock for desktop

echo "Sydney \t\t\t`export TZ='Australia/Sydney';date +'%H:%M';unset TZ`"

echo "Hong Kong \t\t`export TZ='Asia/Hong_Kong';date +'%H:%M';unset TZ`"

echo "Dubai \t\t\t`export TZ='Asia/Dubai';date +'%H:%M';unset TZ`"

echo "Berlin \t\t\t`export TZ='Europe/Berlin';date +'%H:%M';unset TZ`"

echo "New York \t\t`export TZ='America/New_York';date +'%H:%M';unset TZ`"

echo "Grand Cayman \t`export TZ='America/Cayman';date +'%H:%M';unset TZ`"

echo "San Francisco \t`export TZ='America/Los_Angeles';date +'%H:%M';unset TZ`"
